package com.evgeniydrachev.microtasks.core.users;

import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

public class InMemoryUserRepository implements UserRepository {

    private final Map<String, User> users = new TreeMap<>();

    @Override
    public User getOrCreateByEmail(String email) {
        return users.computeIfAbsent(email, key -> new User(email));
    }
}
