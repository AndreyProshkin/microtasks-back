package com.evgeniydrachev.microtasks.core.workspace;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

class TaskBox implements Serializable {
    private final List<Task> tasks = new ArrayList<>();

    public TaskBox() { }

    public void insertFirst(Task task) {
        tasks.add(0, task);
    }

    public void insert(int actualTasksOrder, Task task) {
        int order = actualTasksOrder;
        tasks.add(order, task);
    }

    public Optional<Task> get(UUID uuid) {
        return tasks.stream().filter(taskEl -> taskEl.getUuid().equals(uuid)).findAny();
    }

    public Task remove(UUID uuid) {
        Task foundTask = get(uuid).orElseThrow();
        tasks.remove(foundTask);
        return foundTask;
    }

    public List<Task> getActual() {
        return tasks.stream().filter(task -> !task.isCompleted()).collect(Collectors.toList());
    }

    public List<Task> getCompleted() {
        return tasks.stream().filter(task -> task.isCompleted()).collect(Collectors.toList());
    }
}
