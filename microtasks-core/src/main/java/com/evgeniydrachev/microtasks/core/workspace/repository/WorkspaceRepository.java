package com.evgeniydrachev.microtasks.core.workspace.repository;

import com.evgeniydrachev.microtasks.core.workspace.Workspace;

import java.util.List;
import java.util.UUID;

public interface WorkspaceRepository {
    Workspace getOrCreateDefault(UUID userId);
    void save(Workspace workspace);

    Workspace get(UUID uuid);
    List<Workspace> getAll(UUID userId);

    void remove(Workspace workspace);
}
