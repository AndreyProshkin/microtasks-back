package com.evgeniydrachev.microtasks.core.workspace;

public enum Category {
    TODAY, SOON, LATER
}
