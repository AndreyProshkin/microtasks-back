package com.evgeniydrachev.microtasks.core.users;

public interface UserRepository {
    User getOrCreateByEmail(String email);
}
