package com.evgeniydrachev.microtasks.core.workspace;

import java.io.Serializable;
import java.util.UUID;

public class Task implements Serializable {
    private final UUID uuid;
    private boolean completed;
    private String summary;

    public Task() {
        this.uuid = UUID.randomUUID();
    }

    public Task(String summary) {
        this.uuid = UUID.randomUUID();
        this.summary = summary;
    }

    public Task(UUID uuid, String summary) {
        this.uuid = uuid;
        this.summary = summary;
    }

    public Task(UUID uuid, String summary, boolean completed) {
        this.uuid = uuid;
        this.summary = summary;
        this.completed = completed;
    }

    public String getSummary() {
        return summary;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
