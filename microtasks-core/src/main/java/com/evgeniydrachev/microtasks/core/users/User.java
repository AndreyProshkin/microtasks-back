package com.evgeniydrachev.microtasks.core.users;

import java.util.UUID;

public class User {
    private final UUID id;
    private final String email;

    public User(String email) {
        this.id = UUID.randomUUID();
        this.email = email;
    }

    public User(UUID id, String email) {
        this.id = id;
        this.email = email;
    }

    public UUID getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }
}
