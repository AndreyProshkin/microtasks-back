package com.evgeniydrachev.microtasks.core.users;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class InMemoryUserRepositoryTest {

    @Test
    public void test() {
        InMemoryUserRepository repository = new InMemoryUserRepository();
        User user1 = repository.getOrCreateByEmail("test");
        User user2 = repository.getOrCreateByEmail("test");
        Assertions.assertEquals(user1.getId(), user2.getId());

        User user3 = repository.getOrCreateByEmail("new-test");
        Assertions.assertNotEquals(user1.getId(), user3.getId());
    }
}