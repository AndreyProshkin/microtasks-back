package com.evgeniydrachev.microtasks.core.workspace;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class WorkspaceTest {

    private Workspace workspace;

    @BeforeEach
    void setUp() {
        workspace = new Workspace(UUID.randomUUID());
        addTasks("today-", Category.TODAY);
        addTasks("soon-", Category.SOON);
        addTasks("later-", Category.LATER);
    }

    private void addTasks(String prefix, Category category) {
        for (int i = 3; i >= 0; i--) {
            workspace.addTask(new Task(prefix + i), category);
        }
    }

    @Test
    void updateTask() {
        Task task = findTask(Category.TODAY, "today-2");
        task.setSummary("updated");
        workspace.updateTask(task, Category.SOON, 1);

        List<Task> todayTasks = workspace.getTasks(Category.TODAY);
        assertTasksSummary(todayTasks, List.of("today-0", "today-1", "today-3"));

        List<Task> soonTasks = workspace.getTasks(Category.SOON);
        assertTasksSummary(soonTasks, List.of("soon-0", "updated", "soon-1", "soon-2", "soon-3"));
        Task updatedTask = findTask(Category.SOON, "updated");
        assertEquals(task.getUuid(), updatedTask.getUuid());

        List<Task> laterTasks = workspace.getTasks(Category.LATER);
        assertTasksSummary(laterTasks, List.of("later-0", "later-1", "later-2", "later-3"));
    }

    private Task findTask(Category category, String summary) {
        return workspace.getTasks(category).stream()
                .filter(taskEl -> taskEl.getSummary().equals(summary))
                .findAny().get();
    }

    private void assertTasksSummary(List<Task> tasks, List<String> summaries) {
        assertEquals(summaries.size(), tasks.size());
        for(int i = 0; i < summaries.size(); i++) {
            assertEquals(summaries.get(i), tasks.get(i).getSummary());
        }
    }

    @Test
    void getTasks() {
        List<Task> todayTasks = workspace.getTasks(Category.TODAY);
        assertTasksSummary(todayTasks, List.of("today-0", "today-1", "today-2", "today-3"));

        List<Task> soonTasks = workspace.getTasks(Category.SOON);
        assertTasksSummary(soonTasks, List.of("soon-0", "soon-1", "soon-2", "soon-3"));

        List<Task> laterTasks = workspace.getTasks(Category.LATER);
        assertTasksSummary(laterTasks, List.of("later-0", "later-1", "later-2", "later-3"));
    }

    @Test
    void removeTask() {
        Task task = findTask(Category.TODAY, "today-2");
        workspace.removeTask(task.getUuid());

        List<Task> todayTasks = workspace.getTasks(Category.TODAY);
        assertTasksSummary(todayTasks, List.of("today-0", "today-1", "today-3"));

        List<Task> soonTasks = workspace.getTasks(Category.SOON);
        assertTasksSummary(soonTasks, List.of("soon-0", "soon-1", "soon-2", "soon-3"));

        List<Task> laterTasks = workspace.getTasks(Category.LATER);
        assertTasksSummary(laterTasks, List.of("later-0", "later-1", "later-2", "later-3"));
    }

    @Test
    void removeNonExistingTask() {
        workspace.removeTask(UUID.randomUUID());

        List<Task> todayTasks = workspace.getTasks(Category.TODAY);
        assertTasksSummary(todayTasks, List.of("today-0", "today-1", "today-2", "today-3"));

        List<Task> soonTasks = workspace.getTasks(Category.SOON);
        assertTasksSummary(soonTasks, List.of("soon-0", "soon-1", "soon-2", "soon-3"));

        List<Task> laterTasks = workspace.getTasks(Category.LATER);
        assertTasksSummary(laterTasks, List.of("later-0", "later-1", "later-2", "later-3"));
    }

    @Test
    void completeAndUncompleteTask() {
        Task task = findTask(Category.TODAY, "today-2");
        workspace.completeTask(task.getUuid());

        List<Task> todayTasks = workspace.getTasks(Category.TODAY);
        assertTasksSummary(todayTasks, List.of("today-0", "today-1", "today-3"));

        List<Task> soonTasks = workspace.getTasks(Category.SOON);
        assertTasksSummary(soonTasks, List.of("soon-0", "soon-1", "soon-2", "soon-3"));

        List<Task> laterTasks = workspace.getTasks(Category.LATER);
        assertTasksSummary(laterTasks, List.of("later-0", "later-1", "later-2", "later-3"));

        List<Task> todayCompletedTasks = workspace.getCompletedTasks(Category.TODAY);
        assertTasksSummary(todayCompletedTasks, List.of("today-2"));
        assertEquals(task.getUuid(), todayCompletedTasks.get(0).getUuid());

        workspace.uncompleteTask(task.getUuid());

        todayTasks = workspace.getTasks(Category.TODAY);
        assertTasksSummary(todayTasks, List.of("today-0", "today-1", "today-2", "today-3"));
        Task revertedTask = findTask(Category.TODAY, "today-2");
        assertEquals(task.getUuid(), revertedTask.getUuid());

        soonTasks = workspace.getTasks(Category.SOON);
        assertTasksSummary(soonTasks, List.of("soon-0", "soon-1", "soon-2", "soon-3"));

        laterTasks = workspace.getTasks(Category.LATER);
        assertTasksSummary(laterTasks, List.of("later-0", "later-1", "later-2", "later-3"));

        todayCompletedTasks = workspace.getCompletedTasks(Category.TODAY);
        Assertions.assertEquals(true, todayCompletedTasks.isEmpty());
    }

    @Test
    public void isEmptyWorkspaceEmpty() {
        Workspace workspace = new Workspace(UUID.randomUUID());
        Assertions.assertTrue(workspace.isEmpty());
    }

    @Test
    public void isNotEmptyWorkspaceEmpty() {
        Workspace workspace = new Workspace(UUID.randomUUID());
        workspace.addTask(new Task("test"), Category.SOON);
        Assertions.assertFalse(workspace.isEmpty());
    }
}