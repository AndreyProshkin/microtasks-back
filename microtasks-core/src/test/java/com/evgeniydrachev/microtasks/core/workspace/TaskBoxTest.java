package com.evgeniydrachev.microtasks.core.workspace;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TaskBoxTest {
    @Test
    public void insert() {
        TaskBox box = new TaskBox();
        box.insertFirst(createTask("3", false));
        box.insertFirst(createTask("2", true));
        box.insertFirst(createTask("1", false));
        box.insertFirst(createTask("0", false));

        box.insert(1, new Task("inserted"));

        Assertions.assertEquals("0", box.getActual().get(0).getSummary());
        Assertions.assertEquals("inserted", box.getActual().get(1).getSummary());
        Assertions.assertEquals("1", box.getActual().get(2).getSummary());
    }

    @Test
    public void insertAfterUncompleted() {
        TaskBox box = new TaskBox();
        box.insertFirst(createTask("3", false));
        box.insertFirst(createTask("2", true));
        box.insertFirst(createTask("1", false));
        box.insertFirst(createTask("0", false));

        box.insert(2, new Task("inserted"));

        Assertions.assertEquals("1", box.getActual().get(1).getSummary());
        Assertions.assertEquals("inserted", box.getActual().get(2).getSummary());
        Assertions.assertEquals("3", box.getActual().get(3).getSummary());
    }

    @Test
    public void insertFirst() {
        TaskBox box = new TaskBox();
        box.insertFirst(createTask("1", false));
        box.insertFirst(createTask("0", true));

        box.insert(0, new Task("inserted"));

        Assertions.assertEquals("inserted", box.getActual().get(0).getSummary());
        Assertions.assertEquals("1", box.getActual().get(1).getSummary());
    }

    public Task createTask(String summary, boolean completed) {
        Task task = new Task(summary);
        task.setCompleted(completed);
        return task;
    }
}