package com.evgeniydrachev.microtasks.mongo;

import com.evgeniydrachev.microtasks.core.users.User;
import com.evgeniydrachev.microtasks.core.users.UserRepository;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;

import java.util.UUID;

public class MongoUserRepository implements UserRepository {

    private final MongoCollection<Document> collection;

    public MongoUserRepository(MongoClient client) {

        MongoDatabase database = client.getDatabase("microtasks");
        collection = database.getCollection("users");
    }

    @Override
    public User getOrCreateByEmail(String email) {
        Document document = collection.find(Filters.eq("email", email)).first();
        if(document == null) {
            User user = new User(email);
            collection.insertOne(toDocument(user));
            return user;
        }
        return toUser(document);
    }

    private static User toUser(Document document) {

        UUID id = UUID.fromString(document.getString("_id"));
        String email = document.getString("email");
        return new User(id, email);
    }

    private static Document toDocument(User user) {
        return new Document("_id", user.getId().toString())
                .append("email", user.getEmail());
    }
}
