package com.evgeniydrachev.microtasks.mongo;

import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.MongodConfig;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;

import java.io.Closeable;
import java.io.IOException;
import java.net.UnknownHostException;

public class EmbeddedMongo implements Closeable {
    private int port;
    private MongodExecutable mongodExecutable;

    public EmbeddedMongo() {
        MongodStarter starter = MongodStarter.getDefaultInstance();

        try {
            port = Network.getFreeServerPort();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        MongodConfig mongodConfig = null;
        try {
            mongodConfig = MongodConfig.builder()
                    .version(Version.Main.PRODUCTION)
                    .net(new Net(port, Network.localhostIsIPv6()))
                    .build();
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }

        mongodExecutable = starter.prepare(mongodConfig);
        try {
            mongodExecutable.start();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public int getPort() {
        return port;
    }

    @Override
    public void close() {
        if (mongodExecutable != null) {
            mongodExecutable.stop();
        }
    }
}
