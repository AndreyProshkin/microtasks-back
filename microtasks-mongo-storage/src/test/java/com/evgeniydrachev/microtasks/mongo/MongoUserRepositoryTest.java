package com.evgeniydrachev.microtasks.mongo;

import com.evgeniydrachev.microtasks.core.users.User;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static java.lang.String.format;

class MongoUserRepositoryTest {

    @Test
    public void test() {
        try (EmbeddedMongo mongo = new EmbeddedMongo()) {
            MongoClient mongoClient = MongoClients.create(format("mongodb://localhost:%s", mongo.getPort()));
            MongoUserRepository repository = new MongoUserRepository(mongoClient);

            User user1 = repository.getOrCreateByEmail("test");
            User user2 = repository.getOrCreateByEmail("test");
            Assertions.assertEquals(user1.getId(), user2.getId());

            User user3 = repository.getOrCreateByEmail("new-test");
            Assertions.assertNotEquals(user1.getId(), user3.getId());
        }
    }
}