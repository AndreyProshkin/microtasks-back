package com.evgeniydrachev.microtasks.security;

import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.oauth2.server.resource.web.server.ServerBearerTokenAuthenticationConverter;
import org.springframework.security.web.server.authentication.AuthenticationWebFilter;

public class BearerAuthenticationWebFilter extends AuthenticationWebFilter {
    public BearerAuthenticationWebFilter(ReactiveAuthenticationManager authenticationManager) {
        super(authenticationManager);

        this.setServerAuthenticationConverter(new ServerBearerTokenAuthenticationConverter());
    }
}
