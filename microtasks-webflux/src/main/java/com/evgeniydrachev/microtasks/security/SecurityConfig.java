package com.evgeniydrachev.microtasks.security;

import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;

@EnableWebFluxSecurity
public class SecurityConfig {

    @Bean
    public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http, KeyPairProvider keyPairProvider) {
        http.addFilterAt(new BearerAuthenticationWebFilter(new JwtTokenAuthenticationManager(keyPairProvider)),
                SecurityWebFiltersOrder.AUTHENTICATION);
        http.csrf().disable();

        http.authorizeExchange()
                .pathMatchers("/").permitAll()
                .pathMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .pathMatchers("/api").permitAll()
                .pathMatchers("/api/swagger-ui/**").permitAll()
                .pathMatchers("/v3/**").permitAll()
                .pathMatchers("/api/v1/users/authenticate").permitAll()
                .anyExchange().authenticated();
        return http.build();
    }
}
