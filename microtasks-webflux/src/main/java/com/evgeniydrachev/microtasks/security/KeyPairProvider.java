package com.evgeniydrachev.microtasks.security;

import java.security.KeyPair;

public interface KeyPairProvider {
    KeyPair get();
}
