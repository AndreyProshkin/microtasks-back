package com.evgeniydrachev.microtasks.security;

import java.nio.file.Files;
import java.nio.file.Path;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class FileBasedKeyPairProvider implements KeyPairProvider {

    private final KeyPair keyPair;

    public FileBasedKeyPairProvider(Path privateKeyPath, Path publicKeyPath) {
        PrivateKey privateKey = readPrivateKey(privateKeyPath);
        PublicKey publicKey = readPublicKey(publicKeyPath);

        this.keyPair = new KeyPair(publicKey, privateKey);
    }

    @Override
    public KeyPair get() {
        return keyPair;
    }

    private static PrivateKey readPrivateKey(Path path) {
        try {
            byte[] keyBytes = Files.readAllBytes(path);

            PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
            KeyFactory factory = KeyFactory.getInstance("RSA");
            return factory.generatePrivate(spec);
        } catch (Exception e) {
            throw new RuntimeException("Cannot load private key", e);
        }
    }

    private static PublicKey readPublicKey(Path path) {

        try {
            byte[] keyBytes = Files.readAllBytes(path);

            X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
            KeyFactory factory = KeyFactory.getInstance("RSA");
            return factory.generatePublic(spec);
        } catch (Exception e) {
            throw new RuntimeException("Cannot load public key", e);
        }
    }
}
