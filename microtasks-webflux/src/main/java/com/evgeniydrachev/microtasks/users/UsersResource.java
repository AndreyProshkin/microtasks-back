package com.evgeniydrachev.microtasks.users;

import com.evgeniydrachev.microtasks.core.users.User;
import com.evgeniydrachev.microtasks.core.users.UserRepository;
import com.evgeniydrachev.microtasks.core.workspace.repository.WorkspaceRepository;
import com.evgeniydrachev.microtasks.security.KeyPairProvider;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import io.jsonwebtoken.Jwts;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/users")
public class UsersResource {

    private final GoogleVerifierFacade facade;
    private final UserRepository repository;
    private final WorkspaceRepository workspaceRepository;
    private final KeyPairProvider keyPairProvider;

    public UsersResource(GoogleVerifierFacade facade, UserRepository repository, WorkspaceRepository workspaceRepository, KeyPairProvider keyPairProvider) {
        this.facade = facade;
        this.repository = repository;
        this.workspaceRepository = workspaceRepository;
        this.keyPairProvider = keyPairProvider;
    }

    @PostMapping("/authenticate")
    public AuthenticateResponse authenticate(@RequestBody AuthenticateRequest request) {
        Optional<GoogleIdToken> idTokenHolder = facade.getGoogleIdToken(request.getIdToken());
        GoogleIdToken idToken = idTokenHolder.orElseThrow(() -> new SecurityException("invalid ID token"));

        GoogleIdToken.Payload payload = idToken.getPayload();

        Long expirationTimeSeconds = payload.getExpirationTimeSeconds();
        Instant whenTokenIsExpired = Instant.ofEpochSecond(expirationTimeSeconds);
        Instant now = Instant.now();
        if(now.isAfter(whenTokenIsExpired)) {
            throw new SecurityException("token is expired");
        }

        String email = payload.getEmail();
        boolean emailVerified = Boolean.valueOf(payload.getEmailVerified());
        if(!emailVerified) {
            throw new SecurityException("email is not verified");
        }

        User user = repository.getOrCreateByEmail(email);
        workspaceRepository.getOrCreateDefault(user.getId());

        String jwt = Jwts.builder()
                .setSubject(user.getId().toString())
                .signWith(keyPairProvider.get().getPrivate())
                .compact();

        return new AuthenticateResponse(jwt);
    }
}

class AuthenticateRequest {
    private String idToken;

    public String getIdToken() {
        return idToken;
    }

    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }
}

class AuthenticateResponse {
    private final String token;

    public AuthenticateResponse(String token) {
        this.token = token;
    }

    @JsonProperty("access_token")
    public String getToken() {
        return token;
    }
}
