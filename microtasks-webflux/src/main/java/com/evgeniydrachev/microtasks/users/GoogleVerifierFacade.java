package com.evgeniydrachev.microtasks.users;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Optional;

@Service
public class GoogleVerifierFacade {

    private final String clientId;

    public GoogleVerifierFacade(@Value("${google.client-id}") String clientId) {
        this.clientId = clientId;
    }

    public Optional<GoogleIdToken> getGoogleIdToken(String rawIdToken) {
        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(),
                JacksonFactory.getDefaultInstance())
                .setAudience(Collections.singletonList(clientId))
                .build();

        GoogleIdToken idToken = null;
        try {
            idToken = verifier.verify(rawIdToken);
        } catch (Exception e) {
            throw new RuntimeException("cannot verify token", e);
        }

        return Optional.ofNullable(idToken);
    }
}
