package com.evgeniydrachev.microtasks.workspace;

import com.evgeniydrachev.microtasks.core.workspace.Task;
import com.evgeniydrachev.microtasks.core.workspace.Workspace;
import com.evgeniydrachev.microtasks.core.workspace.repository.WorkspaceRepository;
import io.swagger.v3.oas.annotations.Operation;
import com.evgeniydrachev.microtasks.security.JwtWithUserIdAuthentication;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.evgeniydrachev.microtasks.core.workspace.Category.*;

@RestController
@RequestMapping("/api/v1/workspace")
@Deprecated
public class WorkspaceResource {

    private final WorkspaceRepository repository;

    public WorkspaceResource(WorkspaceRepository repository) {
        this.repository = repository;
    }

    @GetMapping
    @Deprecated
    public List<TaskBox> get(JwtWithUserIdAuthentication authentication) {

        Workspace workspace = repository.getOrCreateDefault(authentication.getUserId());
        List<TaskBox> boxes = new ArrayList<>();
        boxes.add(new TaskBox(TODAY, mapToResponse(workspace.getTasks(TODAY))));
        boxes.add(new TaskBox(SOON, mapToResponse(workspace.getTasks(SOON))));
        boxes.add(new TaskBox(LATER, mapToResponse(workspace.getTasks(LATER))));
        return boxes;
    }

    @PostMapping("/update-task")
    @Operation(summary = "Update the task, changing rank will affect other tasks rank inside a category")
    @Deprecated
    public void updateTask(@RequestBody UpdateTaskRequest request, JwtWithUserIdAuthentication authentication) {
        Workspace workspace = repository.getOrCreateDefault(authentication.getUserId());
        workspace.updateTask(request.getTask(), request.getCategory(), request.getOrder());
        repository.save(workspace);
    }

    @PostMapping("/create-task")
    @Operation(summary = "Create new task, the task will be the first in the category")
    @Deprecated
    public UuidResponse createTask(@RequestBody AddTaskRequest request, JwtWithUserIdAuthentication authentication) {
        Workspace workspace = repository.getOrCreateDefault(authentication.getUserId());
        Task task = new Task(request.getSummary());
        workspace.addTask(task, request.getCategory());
        repository.save(workspace);
        return new UuidResponse(task.getUuid());
    }

    @PostMapping("/complete-task")
    @Operation(summary = "Complete the task with passed id, the task will no longer be returned")
    @Deprecated
    public void completeTask(@RequestBody UuidRequest request, JwtWithUserIdAuthentication authentication) {
        Workspace workspace = repository.getOrCreateDefault(authentication.getUserId());
        workspace.completeTask(request.getUuid());
        repository.save(workspace);
    }

    @GetMapping("/completed-tasks")
    @Operation(summary = "List of completed tasks")
    @Deprecated
    public List<TaskResponse> completedTasks(JwtWithUserIdAuthentication authentication) {
        Workspace workspace = repository.getOrCreateDefault(authentication.getUserId());
        List<TaskResponse> tasks = new ArrayList<>();
        tasks.addAll(mapToResponse(workspace.getCompletedTasks(TODAY)));
        tasks.addAll(mapToResponse(workspace.getCompletedTasks(SOON)));
        tasks.addAll(mapToResponse(workspace.getCompletedTasks(LATER)));
        return tasks;
    }

    private List<TaskResponse> mapToResponse(List<Task> tasks) {
        return tasks.stream().map(TaskResponse::new).collect(Collectors.toList());
    }

    @PostMapping("/uncomplete-task")
    @Operation(summary = "Uncomplete the task with passed id, the task will be returned to workspace")
    @Deprecated
    public void uncompleteTask(@RequestBody UuidRequest request, JwtWithUserIdAuthentication authentication) {
        Workspace workspace = repository.getOrCreateDefault(authentication.getUserId());
        workspace.uncompleteTask(request.getUuid());
        repository.save(workspace);
    }

    @DeleteMapping("/task/{uuid}")
    @Operation(summary = "Remove the task with passed id, the task will no longer be returned")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Deprecated
    public void deleteTask(@PathVariable("uuid") UUID uuid, JwtWithUserIdAuthentication authentication) {
        Workspace workspace = repository.getOrCreateDefault(authentication.getUserId());
        workspace.removeTask(uuid);
        repository.save(workspace);
    }
}
