package com.evgeniydrachev.microtasks.workspace;

import com.evgeniydrachev.microtasks.core.workspace.Category;
import com.evgeniydrachev.microtasks.core.workspace.Task;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class UpdateTaskRequest {
    private final Task task;
    private final Category category;
    private final int order;

    public UpdateTaskRequest(@JsonProperty("uuid") UUID uuid,
                             @JsonProperty("summary") String summary,
                             @JsonProperty("category") Category category,
                             @JsonProperty("order") int order) {
        this.task = new Task(uuid, summary);
        this.category = category;
        this.order = order;
    }

    public Task getTask() {
        return task;
    }

    public Category getCategory() {
        return category;
    }

    public int getOrder() {
        return order;
    }
}
