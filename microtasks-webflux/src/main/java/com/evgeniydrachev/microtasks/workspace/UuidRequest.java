package com.evgeniydrachev.microtasks.workspace;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class UuidRequest {
    private final UUID uuid;

    public UuidRequest(@JsonProperty("uuid") UUID uuid) {
        this.uuid = uuid;
    }

    public UUID getUuid() {
        return uuid;
    }
}
