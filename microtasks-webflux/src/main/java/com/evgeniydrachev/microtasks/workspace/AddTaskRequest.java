package com.evgeniydrachev.microtasks.workspace;

import com.evgeniydrachev.microtasks.core.workspace.Category;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AddTaskRequest {
    private final String summary;
    private final Category category;

    public AddTaskRequest(@JsonProperty("summary") String summary, @JsonProperty("category") Category category) {
        this.summary = summary;
        this.category = category;
    }

    public String getSummary() {
        return summary;
    }

    public Category getCategory() {
        return category;
    }
}
