package com.evgeniydrachev.microtasks.workspace;

import com.evgeniydrachev.microtasks.core.workspace.Category;
import com.evgeniydrachev.microtasks.core.workspace.Task;

import java.util.List;

public class TaskBox {
    private final Category category;
    private final List<TaskResponse> tasks;

    public TaskBox(Category category, List<TaskResponse> tasks) {
        this.category = category;
        this.tasks = tasks;
    }

    public Category getCategory() {
        return category;
    }

    public List<TaskResponse> getTasks() {
        return tasks;
    }
}
