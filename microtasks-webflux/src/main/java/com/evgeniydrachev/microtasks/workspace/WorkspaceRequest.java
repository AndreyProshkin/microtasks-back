package com.evgeniydrachev.microtasks.workspace;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WorkspaceRequest {
    private final String name;

    public WorkspaceRequest(@JsonProperty("name") String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
