package com.evgeniydrachev.microtasks.workspace;

import com.evgeniydrachev.microtasks.core.workspace.Workspace;

import java.util.UUID;

public class WorkspaceResponse {
    private final Workspace workspace;

    public WorkspaceResponse(Workspace workspace) {
        this.workspace = workspace;
    }

    public UUID getUuid() {
        return workspace.getId();
    }

    public String getName() {
        return workspace.getName();
    }
}
