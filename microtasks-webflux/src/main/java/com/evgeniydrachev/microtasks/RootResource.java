package com.evgeniydrachev.microtasks;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.servers.Server;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.reactive.result.view.RedirectView;

@Controller
@OpenAPIDefinition(servers = {
        @Server(url = "https://microtasks.app"),
        @Server(url = "http://localhost:8080")
})
public class RootResource {
    @GetMapping({"/", "/api"})
    public RedirectView indexController() {
        RedirectView redirectView = new RedirectView();
        redirectView.setUrl("/api/swagger-ui/index.html");
        return redirectView;
    }
}
