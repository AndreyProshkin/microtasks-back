package com.evgeniydrachev.microtasks;

import com.evgeniydrachev.microtasks.core.users.UserRepository;
import com.evgeniydrachev.microtasks.core.workspace.WorkspaceService;
import com.evgeniydrachev.microtasks.core.workspace.repository.WorkspaceRepository;
import com.evgeniydrachev.microtasks.mongo.MongoUserRepository;
import com.evgeniydrachev.microtasks.mongo.MongoWorkspaceRepository;
import com.evgeniydrachev.microtasks.security.FileBasedKeyPairProvider;
import com.evgeniydrachev.microtasks.security.KeyPairProvider;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.nio.file.Path;

@Configuration
public class Config {

    @Bean
    public MongoClient mongoClient(@Value("${mongo.connection-string}") String connectionString) {
        MongoClient client = MongoClients.create(connectionString);
        return client;
    }

    @Bean
    public WorkspaceRepository workspaceRepository(MongoClient client) {
        return new MongoWorkspaceRepository(client);
    }

    @Bean
    public WorkspaceService workspaceService(WorkspaceRepository repository) {
        return new WorkspaceService(repository);
    }

    @Bean
    public UserRepository userRepository(MongoClient client) {
        return new MongoUserRepository(client);
    }

    @Bean
    public KeyPairProvider keyPairProvider(
            @Value("${key.private-path}") String privateKeyPath,
            @Value("${key.public-path}") String publicKeyPath
            ) {
        return new FileBasedKeyPairProvider(Path.of(privateKeyPath), Path.of(publicKeyPath));
    }
}
