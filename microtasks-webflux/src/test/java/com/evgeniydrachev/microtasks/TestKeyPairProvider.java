package com.evgeniydrachev.microtasks;

import com.evgeniydrachev.microtasks.security.KeyPairProvider;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import java.security.KeyPair;

public class TestKeyPairProvider implements KeyPairProvider {

    private final KeyPair keyPair = Keys.keyPairFor(SignatureAlgorithm.RS256);


    @Override
    public KeyPair get() {
        return keyPair;
    }
}
