package com.evgeniydrachev.microtasks;

import com.evgeniydrachev.microtasks.core.users.InMemoryUserRepository;
import com.evgeniydrachev.microtasks.core.users.UserRepository;
import com.evgeniydrachev.microtasks.core.workspace.repository.InMemoryWorkspaceRepository;
import com.evgeniydrachev.microtasks.core.workspace.repository.WorkspaceRepository;
import com.evgeniydrachev.microtasks.security.KeyPairProvider;
import com.evgeniydrachev.microtasks.users.GoogleVerifierFacade;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.io.IOException;
import java.util.Optional;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureWebTestClient
public class NewUserStartWorkTest {

    @TestConfiguration
    public static class TestConfig {
        @Bean
        public WorkspaceRepository workspaceRepository() {
            return new InMemoryWorkspaceRepository();
        }

        @Bean
        public UserRepository userRepository() {
            return new InMemoryUserRepository();
        }

        @Bean
        public KeyPairProvider keyPairProvider() {
            return new TestKeyPairProvider();
        }
    }

    @Autowired
    private WebTestClient webClient;

    @MockBean
    private GoogleVerifierFacade facade;

    @Test
    public void authorizeNewUser() throws IOException {
        GoogleIdToken idToken = GoogleTokenUtils.generate("email");

        when(facade.getGoogleIdToken("valid_token")).thenReturn(Optional.of(idToken));

        EntityExchangeResult<byte[]> result = webClient.post()
                .uri("/api/v1/users/authenticate")
                .header("Content-Type", "application/json")
                .bodyValue("""
                        {
                          "idToken": "valid_token"
                        }
                        """)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.access_token").isNotEmpty()
                .returnResult();

        JsonNode response = new ObjectMapper().readTree(result.getResponseBody());
        String token = response.get("access_token").asText();

        EntityExchangeResult<byte[]> workspacesResult = webClient.get()
                .uri("/api/v1/workspaces")
                .header("Authorization", "Bearer " + token)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.length()").isEqualTo(1)
                .jsonPath("$[0].name").isEqualTo("Tasks")
                .returnResult();

        JsonNode workspacesResponse = new ObjectMapper().readTree(workspacesResult.getResponseBody());
        String uuid = workspacesResponse.get(0).get("uuid").asText();

        webClient.get()
                .uri("/api/v1/workspaces/{uuid}", uuid)
                .header("Authorization", "Bearer " + token)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.length()").isEqualTo(3)
                .jsonPath("$[0].category").isEqualTo("TODAY")
                .jsonPath("$[0].tasks.length()").isEqualTo(0)
                .jsonPath("$[1].category").isEqualTo("SOON")
                .jsonPath("$[1].tasks.length()").isEqualTo(0)
                .jsonPath("$[2].category").isEqualTo("LATER")
                .jsonPath("$[2].tasks.length()").isEqualTo(0);
    }
}
