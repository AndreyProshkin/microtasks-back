package com.evgeniydrachev.microtasks;

import com.evgeniydrachev.microtasks.core.users.InMemoryUserRepository;
import com.evgeniydrachev.microtasks.core.users.User;
import com.evgeniydrachev.microtasks.core.users.UserRepository;
import com.evgeniydrachev.microtasks.core.workspace.repository.InMemoryWorkspaceRepository;
import com.evgeniydrachev.microtasks.core.workspace.repository.WorkspaceRepository;
import com.evgeniydrachev.microtasks.security.KeyPairProvider;
import com.evgeniydrachev.microtasks.users.GoogleVerifierFacade;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;


@SpringBootTest
@AutoConfigureWebTestClient
class UsersResourceTest {

    @TestConfiguration
    public static class TestConfig {
        @Bean
        public UserRepository userRepository() {
            return new InMemoryUserRepository();
        }

        @Bean
        public WorkspaceRepository workspaceRepository() {
            return new InMemoryWorkspaceRepository();
        }

        @Bean
        public KeyPairProvider keyPairProvider() {
            return new TestKeyPairProvider();
        }
    }

    @Autowired
    private WebTestClient webClient;

    @MockBean
    private GoogleVerifierFacade facade;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private KeyPairProvider keyPairProvider;

    @Test
    public void tryBadToken() throws Exception {
        webClient.post()
                .uri("/api/v1/users/authenticate")
                .header("Content-Type", "application/json")
                .bodyValue("""
                        {
                          "idToken": "this_is_not_a_token"
                        }
                        """)
                .exchange()
                .expectStatus().isUnauthorized();;
    }

    @Test
    public void authorizeNewUser() {
        GoogleIdToken idToken = GoogleTokenUtils.generate("email");

        when(facade.getGoogleIdToken("valid_token")).thenReturn(Optional.of(idToken));

        webClient.post()
                .uri("/api/v1/users/authenticate")
                .header("Content-Type", "application/json")
                .bodyValue("""
                        {
                          "idToken": "valid_token"
                        }
                        """)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.access_token").isNotEmpty();
    }

    @Test
    public void authorizeUserWithoutVerifiedEmail() {
        GoogleIdToken idToken = GoogleTokenUtils.generate("email", false, false);

        when(facade.getGoogleIdToken("valid_token")).thenReturn(Optional.of(idToken));

        webClient.post()
                .uri("/api/v1/users/authenticate")
                .header("Content-Type", "application/json")
                .bodyValue("""
                        {
                          "idToken": "valid_token"
                        }
                        """)
                .exchange()
                .expectStatus().isUnauthorized();
    }

    @Test
    public void authorizeExistingUser() throws IOException {
        GoogleIdToken idToken = GoogleTokenUtils.generate("existing_email");

        when(facade.getGoogleIdToken("valid_token")).thenReturn(Optional.of(idToken));

        User user = userRepository.getOrCreateByEmail("existing_email");

        EntityExchangeResult<byte[]> result = webClient.post()
                .uri("/api/v1/users/authenticate")
                .header("Content-Type", "application/json")
                .bodyValue("""
                        {
                          "idToken": "valid_token"
                        }
                        """)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.access_token").isNotEmpty()
                .returnResult();

        JsonNode response = new ObjectMapper().readTree(result.getResponseBody());
        String token = response.get("access_token").asText();

        Jws<Claims> jws = Jwts.parserBuilder()
                .setSigningKey(keyPairProvider.get().getPublic())
                .build().parseClaimsJws(token);
        UUID userIdFromToken = UUID.fromString(jws.getBody().getSubject());
        assertEquals(user.getId(), userIdFromToken);
    }
}